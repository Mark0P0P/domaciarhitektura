//
//  SuperHeroSearchUseCase.swift
//  SuperHeroSearch
//
//  Created by Amplitudo on 28/05/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import Foundation

protocol SuperHeroSearchUseCase {
    func search(for term: String, completion: @escaping (Response<SuperHero>) -> Void)
}
