//
//  SuperHeroSearchInteractor.swift
//  SuperHeroSearch
//
//  Created by Amplitudo on 28/05/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import Foundation

class SuperHeroSearchInteractor: SuperHeroSearchUseCase {
    
    let provider: SuperHeroSearchProviderProtocol
    
    init(provider: SuperHeroSearchProviderProtocol) {
        self.provider = provider
    }
    func search(for term: String, completion: @escaping (Response<SuperHero>) -> Void) {
        provider.fetch(for: term, completion: completion)
    }
}
