//
//  Response.swift
//  SuperHeroSearch
//
//  Created by Amplitudo on 28/05/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import Foundation

enum Response<T> {
    case success(T)
    case error(Error)
}
