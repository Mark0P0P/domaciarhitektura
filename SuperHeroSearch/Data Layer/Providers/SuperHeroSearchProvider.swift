//
//  SuperHeroSearchProvider.swift
//  SuperHeroSearch
//
//  Created by Amplitudo on 28/05/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import Foundation

class SuperHeroSearchProvider: SuperHeroSearchProviderProtocol {
    
    let webService: WebService
    
    init(webService: WebService = WebServiceProvider(session: DataNetworkSession())) {
        self.webService = webService
    }
    
    func fetch(for term: String, completion: @escaping (Response<SuperHero>) -> Void) {
        guard let request = ApiRequest(endpoint: .superHeroSearch(superHeroName: term)).urlRequest else { return }
        print(request)
        webService.execute(request) { (response: Response<Result>) in
            switch response {
            case .success(let superHero):
                print(superHero)
                completion(.success(superHero.biography))
            case .error(let error):
                print(error)
            }
        }
    }
}


struct Result: Codable {
    enum CodingKeys: String, CodingKey {
        case biography
    }
    let biography : Biography
}

struct Biography: SuperHero, Codable {
    let fullName: String
    
        enum CodingKeys: String, CodingKey {
            case fullName = "full-name"
}
}

//struct Result: Codable {
//   enum CodingKeys: String, CodingKey {
//        case results = "results"
//    }
//    var results : [Hero]
//}
//
//struct Hero: Codable {
//    enum CodingKeys: String, CodingKey {
//        case biography
//    }
//    let biography: Biography
//}
//
//struct Biography: SuperHero, Codable {
//    let fullName: String
//
//    enum CodingKeys: String, CodingKey {
//        case fullName = "full-name"
//    }
//}
//
