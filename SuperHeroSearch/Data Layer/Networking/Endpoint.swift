//
//  Endpoint.swift
//  SuperHeroSearch
//
//  Created by Amplitudo on 28/05/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import Foundation

enum Endpoint {
    case superHeroSearch(superHeroName: String)
    
    var parameters: String {
        switch self {
        case let .superHeroSearch(superHeroName: superHeroName):
            return "\(superHeroName)"
        }
    }
}
