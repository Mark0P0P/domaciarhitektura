//
//  ApiRequest.swift
//  SuperHeroSearch
//
//  Created by Amplitudo on 28/05/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import Foundation

struct ApiRequest {
    let baseUrlString = "https://superheroapi.com/api/"
    let apiKey = "2281916048566037/"
    let endpoint: Endpoint
    
    init(endpoint: Endpoint) {
        self.endpoint = endpoint
    }
    
    var urlRequest: URLRequest? {
        let urlString = baseUrlString + apiKey + endpoint.parameters
        guard let url = URL(string: urlString) else { return nil }
        return URLRequest(url: url)
        
    }
}

enum RequestError: Error {
    case urlRequestFailed
}

