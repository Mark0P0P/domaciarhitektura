//
//  ResultVC.swift
//  SuperHeroSearch
//
//  Created by Amplitudo on 28/05/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit

protocol ResultVCDependencies {
    var searchUseCase: SuperHeroSearchUseCase { get }
}

class ResultVC: UIViewController {
    
    let searchTerm: String
    let dependencies: ResultVCDependencies
    @IBOutlet weak var heroName: UILabel!
//    var superHero = SuperHero(fullName: "Pff")
    
    init(searchTerm: String, dependencies: ResultVCDependencies){
        self.searchTerm = searchTerm
        self.dependencies = dependencies
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchHero(for: searchTerm)
    }
    
    func fetchHero(for term: String) {
        dependencies.searchUseCase.search(for: term) { [weak self] resp in
            switch resp {
            case .success(let hero):
                self?.heroName.text = hero.fullName
            case .error(let error):
                self?.heroName.text = "slabo si to isprogramirao"
                print(error.localizedDescription)
            }
        }
    }
}
