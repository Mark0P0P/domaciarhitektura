//
//  ResultVCDataSource.swift
//  SuperHeroSearch
//
//  Created by Amplitudo on 28/05/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit

protocol ResultVCDependencies {
    var searchUseCase: SuperHeroSearchUseCase { get }
}

protocol DataSourceUpdateFeedback: class {
    func dataSourceUpdated()
}

class ResultVCDataSource: NSObject {
    let cellID = "movieCell"
    let dependencies: ResultVCDependencies
    var hero = SuperHero()
    weak var feedback: DataSourceUpdateFeedback?
    
    init(dependencies: ResultVCDependencies){
        self.dependencies = dependencies
    }
    
    func fetchSuperHero(for term: String) {
        dependencies.searchUseCase.search(for: term) { [weak self] resp in
            switch resp {
            case .success(let hero):
                self?.hero = hero
                self?.feedback?.dataSourceUpdated()
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
    
}
