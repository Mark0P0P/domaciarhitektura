//
//  AppCoordinator.swift
//  SuperHeroSearch
//
//  Created by Amplitudo on 28/05/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit

class AppCoordinator {
    
    let window: UIWindow
    let dependencies: AppDependenciesContainer
    
    init(window: UIWindow = UIWindow(frame: UIScreen.main.bounds), dependencies: AppDependenciesContainer) {
        self.window = window
        self.dependencies = dependencies
    }
    
    func start() {
        let vc = SearchVC()
        window.rootViewController = vc
        vc.searchDelegate = self
        window.makeKeyAndVisible()
    }
}

extension AppCoordinator: SearchDelegate {
    func userAskedForSearch(with term: String) {
        let resultVC = ResultVC(searchTerm: term, dependencies: dependencies)
        window.rootViewController = resultVC
        window.makeKeyAndVisible()
    }
}

