//
//  SearchVC.swift
//  SuperHeroSearch
//
//  Created by Amplitudo on 28/05/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit

protocol SearchDelegate: class {
    func userAskedForSearch(with term: String)
}

class SearchVC: UIViewController {
    
    @IBOutlet weak var errorLabel: UILabel!
    weak var searchDelegate: SearchDelegate?
    @IBOutlet weak var textField: UITextField!
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        guard let superHeroTerm = textField.text, !superHeroTerm.isEmpty else {
            errorLabel.isHidden = false
            errorLabel.text = "Ne mogu da pretrazujem NISTA! CURANE!!"
            return
        }
        searchDelegate?.userAskedForSearch(with: superHeroTerm)
    }
}

